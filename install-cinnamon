#!/bin/bash
# set -e
##################################################################################################################
# Escrito para usarse en computadoras de 64 bits
# Author 	: 	Héctor Estrada
# Original  :   Erik Dubois
# Website   :   https://gitlab.com/KHelust/install-arch/install-cinnamon
##################################################################################################################
##################################################################################################################
#
#   NO SÓLO LO EJECUTES. ANALIZA Y DECIDE. CORRER BAJO SU PROPIO RIESGO.
#
##################################################################################################################

### Instalar Cinammon
sudo pacman -S --noconfirm cinnamon lightdm lightdm-gtk-greeter

#################################################################################################################

### Programas básicos
sudo pacman -S --noconfirm --needed aspell-es bat biber clipmenu cronie exa fd firefox{,-i18n-es-mx} git hugo hunspell hunspell-es_mx hyphen{,-es} inkscape libreoffice-{fresh,fresh-es} imagemagick materia-gtk-theme mpv mythes-es neofetch neovim nodejs npm ntp odt2txt openssh pacman-contrib pandoc-{citeproc,crossref} papirus-icon-theme pdfarranger perl pulseaudio{,-alsa} python-pip r reflector ripgrep ruby source-highlight sxiv telegram-{desktop,qt} texlive-{bibtexextra,fontsextra,formatsextra,humanities,latexextra,science} translate-shell thunderbird{,-i18n-es-es} vifm wget xdg-user-dirs xorg-{bdftopcf,server,xkill} youtube-dl zathura{,-cb,-djvu,-pdf-mupdf,-ps} zotero zsh{,-autosuggestions,-syntax-highlighting} 

#################################################################################################################

### Programas adicionales para `cinnamon`
sudo pacman -S --noconfirm --needed evince file-roller gimp gparted midori transmission-gtk xed

#################################################################################################################

### Programas para compresión de archivos
sudo pacman -S --noconfirm --needed gzip tar unrar unzip zip

#################################################################################################################

# Instalar programas con otros instaladores de paquetes
sudo npm install -g neovim
sudo npm install -g npm
gem install neovim
gem environment
gem neovim-ruby-host

#################################################################################################################

### Instalar con yay
yay -S ttf-noto-fonts-simple
yay -S jre10-openjdk
yay -S jre10-openjdk-headless
yay -S latex-mk
yay -S spaceship-prompt-git
yay -s texlive-gantt
yay -S tikz-cd
yay -S ufetch-git
yay -S urw-classico
yay -S urw-garamond
yay -S zsh-you-should-use

#################################################################################################################

## Establecer zsh como $SHELL
chsh -s /usr/bin/zsh

#################################################################################################################

## Instalar `FZF`
git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
~/.fzf/install

#################################################################################################################

# Cargar el gestor de pantalla
sudo systemctl enable lightdm.service

#################################################################################################################

### Crear carpeta de repositorios de configuración
mkdir KHelust && cd KHelust

### Descargar repositorios de configuración
git clone https://gitlab.com/KHelust/dotfiles
git clone https://gitlab.com/KHelust/karhec.vim

### Crear los enlaces simbólicos
ln -sr dotfiles/.xprofile ~/
ln -sr dotfiles/.zprofile ~/
ln -sr dotfiles/.zshenv ~/
mkdir ~/.config/i3 -p
ln -sr dotfiles/config/i3/i3.config ~/.config/i3/config
ln -sr dotfiles/config/i3blocks ~/.config/
ln -sr dotfiles/config/neofetch ~/.config/
mkdir ~/.config/nvim -p
ln -sr dotfiles/config/nvim/ftplugin ~/.config/nvim/
ln -sr dotfiles/config/nvim/lugin ~/.config/nvim/
ln -sr dotfiles/config/nvim/spell ~/.config/nvim/
ln -sr dotfiles/config/nvim/init.vim ~/.config/nvim/
ln -sr dotfiles/config/picom ~/.config/
ln -sr dotfiles/config/sxiv ~/.config/
mkdir ~/.config/vifm -p
ln -sr dotfiles/config/vifm/colors ~/.config/vifm/
ln -sr dotfiles/config/vifm/scripts ~/.config/vifm/
ln -sr dotfiles/config/vifm/vifmrc ~/.config/vifm/
ln -sr dotfiles/config/zathura ~/.config/
mkdir ~/.config/zsh -p
ln -sr dotfiles/config/zsh/.zshrc ~/.config/zsh/
ln -sr dotfiles/config/aliases ~/.config/
ln -sr dotfiles/local/bin ~/.local/
sudo ln -srf dotfiles/usr/bin/ufetch /usr/bin/
sudo ln -srf dotfiles/etc/vimrc /etc/
ln -sr karhec.vim/coolor ~/.config/nvim/
ln -sr karhec.vim/autoload/airline ~/.config/nvim/autoload

### Instalar `dmenu`
git clone https://gitlab.com/KHelust/dmenu  
cd dmenu
make  
sudo make install

#################################################################################################################

# Reiniciar
reboot

###############################################################################################

echo "################################################################"
echo "###################    DE Cinnamon Instalado    ################"
echo "################################################################"

exit
